/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
import ImageApp from './ImagesApp'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => ImageApp);
