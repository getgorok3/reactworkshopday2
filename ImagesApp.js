import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View, Button, Alert,
    Image, ScrollView, Modal, TouchableOpacity, TouchableHighlight
} from 'react-native';

export default class App extends Component {
    state = {
        images: '',
        isShowModal: false,
    }
    showModal(visible) {
        this.setState({ isShowModal: visible })
    }
    showImage(image) {
        this.setState({ images: image })
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.header]}>
                    <Text style={[styles.textHeader, styles.text]}>ผลิตภัณท์</Text>
                </View>

                <View style={[styles.row]}>
                    <View style={[styles.column]}>
                        <View style={[styles.pictureContainer]}>
                            <Image source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRN5ncUWwkDOEhJJL3byj8W_O7iEwPoecjjlUktEGCB989ojJhT' }}
                                style={styles.img}></Image>
                            <Modal
                                transparent={true}
                                visible={this.state.isShowModal}
                            >
                                <View style={[styles.row]}>
                                    <View style={[styles.modal]}>
                                        <Button color="#e60000" title="Hide Modal" onPress={() => {
                                            this.showModal(!this.state.isShowModal);
                                        }}></Button>
                                        <Image source={{ uri: this.state.images }}
                                            style={styles.imgModal} resizeMode="stretch"></Image>

                                    </View>
                                </View>
                            </Modal>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRN5ncUWwkDOEhJJL3byj8W_O7iEwPoecjjlUktEGCB989ojJhT')
                                }}
                            >
                                <Text style={styles.name}>แอปเปิ้ล</Text>
                                <Text>80 บาท / 20 ลูก</Text>
                            </TouchableOpacity>

                        </View>
                        <View style={[styles.pictureContainer]}>
                            <Image source={{ uri: 'https://images-na.ssl-images-amazon.com/images/I/51TcdS9z2fL._SY300_QL70_.jpg' }}
                                style={styles.img}></Image>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://images-na.ssl-images-amazon.com/images/I/51TcdS9z2fL._SY300_QL70_.jpg')
                                }}
                            >
                                <Text style={styles.name}>ส้ม</Text>
                                <Text>60 บาท / 30 ลูก</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.pictureContainer]}>
                            <Image source={{ uri: 'https://image.shutterstock.com/image-photo/bunch-bananas-isolated-on-white-260nw-1072013369.jpg' }}
                                style={styles.img}></Image>

                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://image.shutterstock.com/image-photo/bunch-bananas-isolated-on-white-260nw-1072013369.jpg')
                                }}
                            >
                                <Text style={styles.name}>กล้วย</Text>
                                <Text>40 บาท / 1 หวี</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={[styles.column]}>
                        <View style={[styles.pictureContainer]}>
                            <Image source={{ uri: 'https://food.fnr.sndimg.com/content/dam/images/food/fullset/2018/5/16/0/FNK_WATERMELON-PIG-H_s4x3.jpg.rend.hgtvcom.826.620.suffix/1526483893553.jpeg' }}
                                style={styles.img}></Image>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://food.fnr.sndimg.com/content/dam/images/food/fullset/2018/5/16/0/FNK_WATERMELON-PIG-H_s4x3.jpg.rend.hgtvcom.826.620.suffix/1526483893553.jpeg')
                                }}
                            >
                                <Text style={styles.name}>แตงโม</Text>
                                <Text>20 บาท / 1 ลูก</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.pictureContainer]}>
                            <Image source={{ uri: 'https://www.coopathome.ch/img/produkte/300_300/RGB/4903240_001.jpg?_=1457790108707' }}
                                style={styles.img}></Image>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://www.coopathome.ch/img/produkte/300_300/RGB/4903240_001.jpg?_=1457790108707')
                                }}
                            >
                                <Text style={styles.name}>มะละกอ</Text>
                                <Text>15 บาท / 1ล ูก</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.pictureContainer]}>
                            <Image source={{ uri: 'https://s3.amazonaws.com/cdn.gurneys.com/images/475/13853.jpg' }}
                                style={styles.img}></Image>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://s3.amazonaws.com/cdn.gurneys.com/images/475/13853.jpg')
                                }}
                            >
                                <Text style={styles.name}>สตอเบอรี่</Text>
                                <Text>50 บาท / 1 กล่อง</Text>
                            </TouchableOpacity>
                        </View>


                    </View>

                </View>
                <View style={styles.footer}>
                    <View style={[styles.footerRow]}>
                        <Text style={[styles.footerColumn]}>หน้าหลัก</Text>
                        <Text style={[styles.footerColumn]}>สินค้า</Text>
                        <Text style={[styles.footerColumn]}>ตะกร้า</Text>
                        <Text style={[styles.footerColumn]}>แจ้งเตือน</Text>
                        <Text style={[styles.footerColumn]}>ฉัน</Text>
                    </View>
                    <View >
                    <Button color="black"title=""></Button>
                    </View>
                </View>


            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    header: {
        backgroundColor: '#0099ff',
        height: 35
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        // backgroundColor: 'red',
        justifyContent: 'center',

    },
    column: {
        flex: 1,
        flexDirection: 'column',
        // backgroundColor: 'blue',
        margin: 10,


    },
    
    textHeader: {
        color: 'white'
    },
    text: {
        textAlignVertical: 'center',
        textAlign: 'center',
        fontSize: 25
    },
    pictureContainer: {
        width: 120,
        height: 120,
        marginBottom: 50,
        marginLeft: 30

    },
    img: {
        borderRadius: 10,
        width: '100%', height: '100%',
        alignItems: 'center'

    },
    footer: {
        // backgroundColor: 'gold',
        width: '100%',
       
    },
    modal: {
        marginTop: 30,
        width: '80%',
        height: '80%',
        backgroundColor: 'white',
    },
    imgModal: {
        flex: 1,
    },
    footerRow: {
        flexDirection: 'row',
        marginLeft: '5%',
        marginRight: '5%'
        
    },
    footerColumn: {
        marginLeft: '2%',
        marginRight: '5%',
        flexDirection: 'column',
        fontSize: 18,
        color: 'black',
  
    },
    name: {
        fontWeight: 'bold',
        color: 'black'
    },
   


})