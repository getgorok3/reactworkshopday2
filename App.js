/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import {
  Platform, StyleSheet, Text, View, Button, Alert, TouchableHighlight,
  Modal, TextInput, Image, TouchableOpacity
} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  state = {
    username: '',
    password: '',
    isShowModal: false,
    validateMessage: ''

  }
  showModal(visible) {
    this.setState({ isShowModal: visible })
  }
  isCorrectPass = () => {
    if (this.state.username.toLowerCase() === 'admin' && this.state.password.toLowerCase() === 'eiei') {
      this.setState({ validateMessage: 'The username and password is correct' })
      this.showModal(true)
    } else {
      this.setState({ validateMessage: 'The username and password is incorrect' })
      this.showModal(true)
    }
  }
  render() {
    return (    //justify center make TextInput hard to show ex align center make width disappear
      <View style={styles.container}>
        <View style={[styles.row, styles.center]}>
          <View style={[styles.pictureContainer]}>
            <Image source={{ uri: 'https://modernfarmer.com/wp-content/uploads/2015/09/alpacayawn1.jpg' }}
              style={styles.img}></Image>
          </View>
        </View>
        <View style={[styles.row2]}>
          <Text style={styles.userText}> {this.state.username} - {this.state.password} </Text>
          <View style={[styles.inputContainer,]}>
            <TextInput value={this.state.username} style={styles.inputBox}
              onChangeText={(username) => this.setState({ username })} />
            <TextInput value={this.state.password} style={styles.inputBox}
              onChangeText={(password) => this.setState({ password })} />
          </View>

          <Modal
            transparent={true}
            visible={this.state.isShowModal}
          >
            <View style={[styles.row,styles.center]}>
              <View style={[styles.modal]}>
                <Text style={styles.validateText}>{this.state.validateMessage}</Text>
                <Button  style={styles.modalButton}color="#e60000" title="Hide Modal" onPress={() => {
                  this.showModal(!this.state.isShowModal);
                }}></Button>
              </View>
            </View>
          </Modal>
          <View style={styles.opacityContainer}>
            <TouchableOpacity
              style={styles.opacity}
              onPress={this.isCorrectPass}
            >
              <Text style={styles.text}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#696969'
  },
  row2: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#696969'
  },
  img: {
    borderRadius: 150,
    width: '100%', height: '100%',

  },
  pictureContainer: {
    width: 200,
    height: 200,

  },
  center: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  inputBox: {
    height: 40, borderColor: 'gray', borderWidth: 1, width: '90%', backgroundColor: 'white',
    margin: 5
  },
  inputContainer: {
    flex: 1,
    alignItems: 'center'
  },
  opacityContainer: {
    flex: 1,
    alignItems: 'center',
  },
  opacity: {
    width: '90%', backgroundColor: 'black', height: 40,

  },
  text: {
    textAlign: 'center',
    fontSize: 25,
    color: 'white'
  },
  userText: {
    marginLeft: 20,
    fontSize: 14,
    color: 'white'
  },
  modal: {
    
    margin: 45,
    width: 300,
    height: 50,
    backgroundColor: 'white',
  
  },
  validateText:{
    fontSize: 14,
    textAlign: 'center'
  }
  
 
});
